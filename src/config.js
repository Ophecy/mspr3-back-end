/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */
if (process.env.NODE_ENV !== 'production') {
  try {
    const dotenv = require('dotenv');
    dotenv.config();
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Le package dotenv est manquant. Executez la commande yarn install et réessayez.');
    process.exit(666);
  }
}

const dbhost = process.env.DB_HOST;
const dbuser = process.env.DB_USER;
const dbpassword = process.env.DB_PASSWORD;
const dbname = process.env.DB_NAME;
module.exports = {
  port: '8080',
  knexconfig: {
    client: 'mysql', // changer le type de base de données en fonction de l'arcitecture presente
    connection: {
      host: dbhost,
      user: dbuser,
      password: dbpassword,
      database: dbname,
    },
    useNullAsDefault: true,
  },
};
