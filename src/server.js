const restify = require('restify');
const config = require('./config');
const clients = require('./handleClients');
const employees = require('./handleEmployees');

const server = restify.createServer({
  name: 'mspr3_back_end',
});
server.use(restify.plugins.bodyParser());

// routes clients
server.get('/client/:id', clients.get); //* fonctionnel
server.get('/client', clients.get); //* fonctionnel
server.post('/client', clients.add); //* fonctionnel
server.put('/client/:id', clients.update); //* fonctionnel
server.del('/client/:id', clients.del); //* fonctionnel

// routes employés
server.get('/employee/:id', employees.get); //* fonctionnel
server.get('/employee', employees.get); //* fonctionnel
server.post('/employee', employees.add); //* fonctionnel
server.put('/employee/:id', employees.update); //* fonctionnel
server.del('/employee/:id', employees.del); //* fonctionnel

// demarrage serveur
server.listen(config.port, () => {
  // eslint-disable-next-line no-console
  console.log('Server %s listening at %s', server.name, server.url);
});
