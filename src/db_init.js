const mysql = require('mysql');
const fs = require('fs');
const readline = require('readline');
const config = require('./config');

// eslint-disable-next-line no-console
console.log('Cet utilitaire de création de table n\'est pour l\'instant fonctionnel qu\'avec MySQL ou MariaDB.');

let myCon;
try {
  myCon = mysql.createConnection({
    host: config.knexconfig.connection.host,
    port: '3306',
    database: config.knexconfig.connection.database,
    user: config.knexconfig.connection.user,
    password: config.knexconfig.connection.password,
  });
} catch (error) {
  // eslint-disable-next-line no-console
  console.error("une erreur est survenue lors de la communication avec la base de données. Assurez-vous d'avoir bien complété correctement le fichier src/config.js");
  process.exit(666);
}
let rl;
try {
  rl = readline.createInterface({
    input: fs.createReadStream('src/i1_mspr3.sql'),
    terminal: false,
  });
} catch (error) {
  // eslint-disable-next-line no-console
  console.error('Le fichier de creation de tables est introuvable, veuillez retelecharger et reessayer');
  process.exit(666);
}

rl.on('line', (chunk) => {
  myCon.query(chunk.toString('ascii'), (err) => {
    // eslint-disable-next-line no-console
    if (err) console.log(err);
  });
});

rl.on('close', () => {
  // eslint-disable-next-line no-console
  console.log('finished');
  myCon.end();
});
