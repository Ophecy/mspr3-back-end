const db = require('./db');

const get = (req, res, next) => {
  // check if id is provided
  if (req.params.id === undefined) {
    // if not : get all clients
    db('clients')
      .select()
      .then((clients) => {
        res.send({
          clients,
        });
        next();
      });
    return;
  }

  // else : return the right client
  db('clients')
    .select()
    .where({ id: req.params.id })
    .then((client) => {
      res.send({
        client,
      });
      next();
    });
};

const add = (req, res, next) => {
  const params = {
    nom: req.params.nom,
    prenom: req.params.prenom,
    age: req.params.age,
    profession: req.params.profession,
    adresse: req.params.adresse,
    ville: req.params.ville,
    cp: req.params.cp,
    mail: req.params.mail,
    tel: req.params.tel,
  };
  db('clients')
    .insert(params)
    .then((id) => {
      db.select()
        .table('clients')
        .where({ id })
        .then((client) => {
          res.send({
            client,
          });
          next();
        });
    });
};

const update = (req, res, next) => {
  const params = {
    nom: req.params.nom,
    prenom: req.params.prenom,
    age: req.params.age,
    profession: req.params.profession,
    adresse: req.params.adresse,
    ville: req.params.ville,
    cp: req.params.cp,
    mail: req.params.mail,
    tel: req.params.tel,
  };
  db('clients')
    .where({ id: req.params.id })
    .update(params)
    .then(() => {
      db('clients')
        .where({ id: req.params.id })
        .select()
        .then((client) => {
          res.send({
            client,
          });
          next();
        });
    });
};

const eradicate = (req, res, next) => {
  db('clients')
    .where({ id: req.params.id })
    .del()
    .then((nb) => {
      res.send({
        message: `${nb} Client(e)(s) supprimé(e)(s)`,
      });
      next();
    });
};

module.exports = {
  add,
  get,
  update,
  del: eradicate,
};
