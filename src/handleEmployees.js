const db = require('./db');

const get = (req, res, next) => {
  // check if id is provided
  if (req.params.id === undefined) {
    // if not : get all employees
    db('employees')
      .select()
      .then((employees) => {
        res.send({
          employees,
        });
        next();
      });
    return;
  }

  // else : return the right employee
  db('employees')
    .select()
    .where({ id: req.params.id })
    .then((employee) => {
      res.send({
        employee,
      });
      next();
    });
};

const add = (req, res, next) => {
  const params = {
    nom: req.params.nom,
    prenom: req.params.prenom,
    age: req.params.age,
    fonction: req.params.fonction,
    adresse: req.params.adresse,
    ville: req.params.ville,
    cp: req.params.cp,
    mail: req.params.mail,
    tel: req.params.tel,
    photo: req.params.photo,
    manager: req.params.manager,
    service: req.params.service,
  };
  db('employees')
    .insert(params)
    .then((id) => {
      db.select()
        .table('employees')
        .where({ id })
        .then((employee) => {
          res.send({
            employee,
          });
          next();
        });
    });
};

const update = (req, res, next) => {
  const params = {
    nom: req.params.nom,
    prenom: req.params.prenom,
    age: req.params.age,
    fonction: req.params.fonction,
    adresse: req.params.adresse,
    ville: req.params.ville,
    cp: req.params.cp,
    mail: req.params.mail,
    tel: req.params.tel,
    photo: req.params.photo,
    manager: req.params.manager,
    service: req.params.service,
  };
  db('employees')
    .where({ id: req.params.id })
    .update(params)
    .then(() => {
      db.select()
        .table('employees')
        .where({ id: req.params.id })
        .then((employee) => {
          res.send({
            employee,
          });
          next();
        });
    });
};

const eradicate = (req, res, next) => {
  db('employees')
    .where({ id: req.params.id })
    .del()
    .then((nb) => {
      res.send({
        message: `${nb} Employé(e)(s) supprimé(e)(s)`,
      });
      next();
    });
};

module.exports = {
  add,
  get,
  update,
  del: eradicate,
};
