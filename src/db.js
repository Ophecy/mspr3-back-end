const knex = require('knex');
const config = require('./config');

let db;
try {
  db = knex(config.knexconfig);
} catch (error) {
  // eslint-disable-next-line no-console
  console.error("une erreur est survenue lors de la communication avec la base de données. Assurez-vous d'avoir bien complété correctement le fichier src/config.js");
  process.exit(666);
}

module.exports = db;
